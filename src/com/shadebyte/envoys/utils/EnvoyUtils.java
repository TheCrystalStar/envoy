package com.shadebyte.envoys.utils;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.shadebyte.envoys.Envoy;

public class EnvoyUtils {

	private static EnvoyUtils utils = new EnvoyUtils();

	public static EnvoyUtils getUtils() {
		return utils;
	}

	// Create a random instance;
	private Random random = new Random();

	// Randomized envoy chest locations
	public ArrayList<Location> spawnedChest = new ArrayList<>();

	/**
	 * This synchronized method shows all the chest that are currently placed as
	 * emerald blocks, so the user may remove / add or simply see physically
	 * where chest would be spawned.
	 */
	public void showChestLocations() {

		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				loc.getBlock().setType(Material.EMERALD_BLOCK);
			}
		}
	}

	/**
	 * This method hides all of the placed chest this is called after the player
	 * exits the setting chest mode, also synchronized
	 */
	public void hideChestLocations() {

		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				loc.getBlock().setType(Material.AIR);
			}
		}
	}
	
	public void loadPlacedLocations() {

		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				Envoy.getEnvoy().placedChest.add(loc);
			}
		}
	}

	/**
	 * This method is used to spawn a controlled envoy by controlled envoy, it
	 * means that it uses the manual set chest locations, it will not spawn
	 * using the random system.
	 */
	@SuppressWarnings("deprecation")
	public synchronized void spawnControlledEnvoy() {

		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				loc.getBlock().setType(Material.CHEST);
				Chest chest = (Chest) loc.getBlock().getState();

				@SuppressWarnings("unchecked")
				ArrayList<ItemStack> items = (ArrayList<ItemStack>) Envoy.getEnvoy().getConfig().get("items");

				if (Envoy.getEnvoy().getConfig().getBoolean("chest-items.random-amount")) {
					for (int j = 0; j < Maths.getMaths().randInt(
							Envoy.getEnvoy().getConfig().getInt("chest-items.random-range.min"),
							Envoy.getEnvoy().getConfig().getInt("chest-items.random-range.max")); j++) {

						chest.getBlockInventory().addItem(items.get(random.nextInt(items.size())));
					}
				} else {
					for (int j = 0; j < Envoy.getEnvoy().getConfig().getInt("chest-items.set-amount"); j++) {

						chest.getBlockInventory().addItem(items.get(random.nextInt(items.size())));
					}
				}
			}

			Bukkit.getScheduler().runTaskLater(Envoy.getEnvoy(), new BukkitRunnable() {

				@Override
				public void run() {
					removeControlledEnvoyChest();
					this.cancel();
				}
			}, 20 * Envoy.getEnvoy().getConfig().getInt("chest-auto-remove-time"));
		}
	}

	/*
	 * Remove all the controlled envoy chest.
	 */
	public void removeControlledEnvoyChest() {

		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				Material block = loc.getBlock().getType();
				if (block.equals(Material.CHEST)) {
					Chest chest = (Chest) loc.getBlock().getState();
					chest.getBlockInventory().clear();
					loc.getBlock().setType(Material.AIR);
				}
			}
		}
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public synchronized void spawnRandomizedEnvoy() {

		ConfigurationSection sec = Envoy.getEnvoy().getEnvoyLocations().getConfig()
				.getConfigurationSection("locations");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {

				String world = Envoy.getEnvoy().getEnvoyLocations().getConfig()
						.getString("locations." + keys + ".1.world");

				int x1 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".1.x");
				int y1 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".1.y");
				int z1 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".1.z");
				int x2 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".2.x");
				int y2 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".2.y");
				int z2 = Envoy.getEnvoy().getEnvoyLocations().getConfig().getInt("locations." + keys + ".2.z");

				Location locationOne = new Location(Bukkit.getWorld(world), x1, y1, z1);
				Location locationTwo = new Location(Bukkit.getWorld(world), x2, y2, z2);

				for (int i = 0; i < Envoy.getEnvoy().getEnvoyLocations().getConfig()
						.getInt("locations." + keys + ".max-chest"); i++) {

					int x = Maths.getMaths().getRandomLocation(locationOne, locationTwo).getBlockX();
					int z = Maths.getMaths().getRandomLocation(locationOne, locationTwo).getBlockZ();
					int y = locationOne.getWorld().getHighestBlockAt(x, z).getY();

					Location selectedLocation = new Location(Bukkit.getWorld(world), x, y, z);
					selectedLocation.getBlock().setType(Material.CHEST);

					Chest chest = (Chest) selectedLocation.getBlock().getState();

					ArrayList<ItemStack> items = (ArrayList<ItemStack>) Envoy.getEnvoy().getItemsConfig().getConfig()
							.get("items");

					if (Envoy.getEnvoy().getConfig().getBoolean("chest-items.random-amount")) {
						for (int j = 0; j < Maths.getMaths().randInt(
								Envoy.getEnvoy().getConfig().getInt("chest-items.random-range.min"),
								Envoy.getEnvoy().getConfig().getInt("chest-items.random-range.max")); j++) {
							if (items != null) {
								int rand = random.nextInt(items.size());
								if (items.get(rand) != null) {
									chest.getBlockInventory().addItem(items.get(rand));
								}
							}
						}
					} else {
						for (int j = 0; j < Envoy.getEnvoy().getConfig().getInt("chest-items.set-amount"); j++) {
							if (items != null) {
								int rand = random.nextInt(items.size());
								if (items.get(rand) != null) {
									chest.getBlockInventory().addItem(items.get(rand));
								}
							}
						}
					}

					spawnedChest.add(selectedLocation);
				}

			}

			Bukkit.getServer().getScheduler().runTaskLater(Envoy.getEnvoy(), new BukkitRunnable() {

				@Override
				public void run() {
					removeRandomizedEnvoyChest();
					spawnedChest.clear();
				}
			}, 20 * Envoy.getEnvoy().getConfig().getInt("chest-auto-remove-time"));
		}

	}

	public void removeRandomizedEnvoyChest() {

		for (Location loc : spawnedChest) {
			Material block = loc.getBlock().getType();
			if (block.equals(Material.CHEST)) {
				Chest chest = (Chest) loc.getBlock().getState();
				chest.getBlockInventory().clear();
				loc.getBlock().setType(Material.AIR);
			}
		}

	}
}
