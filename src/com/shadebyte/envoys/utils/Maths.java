package com.shadebyte.envoys.utils;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Maths {

	/**
	 * Create a private instance of the maths class.
	 */
	private static Maths maths = new Maths();

	/**
	 * Get the Maths instance;
	 */
	public static Maths getMaths() {
		return maths;
	}

	// Create a random instance;
	private Random rand = new Random();

	/**
	 * @param min
	 *            is the minimum number the generator will start from
	 * @param max
	 *            is the maximum number the generator can produce.
	 */
	public int randInt(int min, int max) {

		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	/**
	 * @param from
	 *            Where is the Object starting from?
	 * @param to
	 *            Where is the Object (wanting) to go to.
	 */
	public Vector goToLocation(Location from, Location to) {
		Vector f = new Vector(from.getX(), from.getY(), from.getZ());
		Vector t = new Vector(to.getX(), to.getY(), to.getZ());
		return t.subtract(f);
	}

	/**
	 * This methods is used to return a random location between two location
	 * points or a cuboid.
	 * 
	 * @param start
	 *            is the first corner of the cuboid
	 * @param end
	 *            is the second corner of the cuboid.
	 */

	public Location getRandomLocation(Location start, Location end) {

		int maxX = Math.max(start.getBlockX(), end.getBlockX());
		int maxY = Math.max(start.getBlockY(), end.getBlockY());
		int maxZ = Math.max(start.getBlockZ(), end.getBlockZ());

		int minX = Math.min(start.getBlockX(), end.getBlockX());
		int minY = Math.min(start.getBlockY(), end.getBlockY());
		int minZ = Math.min(start.getBlockZ(), end.getBlockZ());

		return new Location(start.getWorld(), randInt(minX, maxX), randInt(minY, maxY), randInt(minZ, maxZ));
	}

	/**
	 * Check if the number entered is an Integer number
	 * 
	 * @param string
	 *            is the input you must put to check if that string is a Integer
	 *            number (whole number)
	 */

	public boolean isInt(String string) {
		
		try {
			Integer.parseInt(string);
		} catch(NumberFormatException e) {
			return false;
		}
		
		return true;
	}
}
