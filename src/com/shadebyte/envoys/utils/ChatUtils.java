package com.shadebyte.envoys.utils;

import org.bukkit.ChatColor;

public class ChatUtils {

	/**
	 * Create an instance for the chat utils class.
	 */
	private static ChatUtils chatutils = new ChatUtils();

	/**
	 * Get the instance
	 */
	public static ChatUtils getChatUtils() {
		return chatutils;
	}

	/**
	 * 
	 * @param text
	 *            is the text required to change it to color coded format This
	 *            method is used to change the text to colored.
	 */
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
}
