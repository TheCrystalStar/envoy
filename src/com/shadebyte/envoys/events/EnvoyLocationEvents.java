package com.shadebyte.envoys.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.utils.ChatUtils;

public class EnvoyLocationEvents implements Listener {

	/**
	 * This event handler is used to check when the player left or right clicks
	 * to set the region for where envoy chest should be spawned.
	 */
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent e) {

		// Create a player object
		Player p = e.getPlayer();

		// Check if they are in region selection mode.
		if (!Envoy.getEnvoy().creatingRegion.contains(p))
			return;

		// Checking if the action is a left click
		// if so then add the location to the corner one hashmap
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Envoy.getEnvoy().regionCornerOne.put(p, e.getClickedBlock().getLocation());
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.corner-one-selected")));
		}

		// Checking if the action is a right click
		// if so then add the location to the corner two hashmap
		else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Envoy.getEnvoy().regionCornerTwo.put(p, e.getClickedBlock().getLocation());
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.corner-two-selected")));
			// We are returning right here just to stop the code until
			// they click once more to re-activate.
			return;
		}
	}
}
