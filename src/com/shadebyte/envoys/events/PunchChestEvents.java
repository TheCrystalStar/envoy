package com.shadebyte.envoys.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class PunchChestEvents implements Listener {
	@EventHandler
	public void playerInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			Block block = event.getClickedBlock();
			Player player = event.getPlayer();
			Location location = block.getLocation();
			if (block.getType().equals(Material.CHEST)) {
				if (!Envoy.getEnvoy().settingChestLocations.contains(player)) {
					if (Envoy.getEnvoy().placedChest.contains(location)) {
						block.setType(Material.AIR);
					}
				}
				for (Location loc : EnvoyUtils.getUtils().spawnedChest) {
					Material spawnedChest = loc.getBlock().getType();
					if (spawnedChest.equals(Material.CHEST)) {
						if (loc.equals(block.getLocation())) {
							block.setType(Material.AIR);
						}
					}
				}
			}
		}
	}
}
