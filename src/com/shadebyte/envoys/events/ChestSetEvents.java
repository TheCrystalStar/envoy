package com.shadebyte.envoys.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class ChestSetEvents implements Listener {

	/**
	 * This method is checking when a block is placed when in setting chest
	 * mode, if they are in setting chest mode and they place a chest, the chest
	 * location will be saved to a temporary arraylist.
	 */
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {

		Player p = e.getPlayer();

		if (!Envoy.getEnvoy().settingChestLocations.contains(p))
			return;

		Location location = e.getBlockPlaced().getLocation();
		Envoy.getEnvoy().placedChest.add(location);
		p.sendMessage(ChatUtils.getChatUtils().color("&e&l(!) &eChest location saved!"));
	}

	/**
	 * This method is checking when a block is broken when in setting chest
	 * mode, if they are in setting chest mode and they broke a block that's in
	 * the arraylist then it removes it from the array.
	 * Its also for punching chests and breaking them when envoys are there.
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {

		Player p = e.getPlayer();
		Location location = e.getBlock().getLocation();
		
		if (!Envoy.getEnvoy().settingChestLocations.contains(p)) {
			return;
		}

		if (Envoy.getEnvoy().placedChest.contains(location)) {
			Envoy.getEnvoy().placedChest.remove(location);
			p.sendMessage(ChatUtils.getChatUtils().color("&e&l(!) &eChest location removed!"));
		}

		/*
		 * I'm looping through all the nodes (if any) under the 'chest' node if
		 * it contains a location from a block that was broken, then remove it
		 * from the list completly.
		 */
		ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
		if (sec != null) {
			for (String keys : sec.getKeys(false)) {
				int x = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".x");
				int y = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".y");
				int z = Envoy.getEnvoy().getChestConfig().getConfig().getInt("chest." + keys + ".z");
				String worldn = Envoy.getEnvoy().getChestConfig().getConfig().getString("chest." + keys + ".world");
				World world = Bukkit.getWorld(worldn);
				Location loc = new Location(world, x, y, z);
				if (e.getBlock().getLocation().equals(loc)) {
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest." + keys, null);
					Envoy.getEnvoy().getChestConfig().saveConfig();
				}
			}
		}
	}

	/**
	 * This method is used to controll the events when the player right clicks
	 * an envoy summon stick, it spawns the envoy and removes the item from
	 * their hand.
	 */
	
	@EventHandler
	public void onPlayerUseStick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() != Action.RIGHT_CLICK_AIR) {
			return;
		}

		if (!p.getItemInHand().isSimilar(Envoy.getEnvoy().createItemStack("summonstick"))) {
			return;
		}

		if (Envoy.getEnvoy().getConfig().getBoolean("chest.randomized-locations")) {
			for (String all : Envoy.getEnvoy().getConfig().getStringList("messages.envoy-by-player")) {
				Bukkit.broadcastMessage(ChatUtils.getChatUtils().color(all).replace("{player}", p.getName()));
			}
			EnvoyUtils.getUtils().spawnRandomizedEnvoy();
		} else {
			for (String all : Envoy.getEnvoy().getConfig().getStringList("messages.envoy-by-player")) {
				Bukkit.broadcastMessage(ChatUtils.getChatUtils().color(all).replace("{player}", p.getName()));
			}
			EnvoyUtils.getUtils().spawnControlledEnvoy();
		}

		if (p.getItemInHand().getAmount() == 1) {
			p.setItemInHand(null);
			p.updateInventory();
		} else {
			p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
			p.updateInventory();
		}
	}
}
