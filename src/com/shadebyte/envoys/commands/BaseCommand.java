package com.shadebyte.envoys.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.shadebyte.envoys.utils.ChatUtils;


public abstract class BaseCommand implements CommandExecutor {

    private final String command;
    private final String permission;

    protected BaseCommand(String command, String permission) {
        this.command = command;
        this.permission = permission;
    }

    public String getName() {
        return command;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    	
        if (!sender.hasPermission(permission)) {
        	sender.sendMessage(ChatUtils.getChatUtils().color("&cYou do not have permission to use that command!"));
            return true;
        }

        execute(sender, args);
        return true;
    }

    public abstract void execute(CommandSender sender, String[] args);
}