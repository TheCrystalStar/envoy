package com.shadebyte.envoys.commands.subcmds;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;

public class CreateRegionCMD extends SubCommand {

	public CreateRegionCMD() {
		super("create", "create", "Envoy.cmd.create", "create region", 2);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (!(sender instanceof Player)) {
			return;
		}

		Player p = (Player) sender;

		if (args.length == 1) {
			p.sendMessage(ChatUtils.getChatUtils().color("&e/envoy create <name>"));
		}

		if (args.length == 2) {

			// Checking if the player has selected the first location,
			// if not return and send a message.
			if (!Envoy.getEnvoy().regionCornerOne.containsKey(p)) {
				p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.select-corner-one")));
				return;
			}

			// Checking if the player has selected the first location,
			// if not return and send a message.
			if (!Envoy.getEnvoy().regionCornerTwo.containsKey(p)) {
				p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.select-corner-two")));
				return;
			}

			if (Envoy.getEnvoy().getEnvoyLocations().getConfig().contains("locations." + args[1].toLowerCase())) {
				p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.region-exist-already")));
				return;
			}

			// Get the locations from the hasmaps and give them their own
			// variable names.
			Location locationOne = Envoy.getEnvoy().regionCornerOne.get(p);
			Location locationTwo = Envoy.getEnvoy().regionCornerTwo.get(p);

			// Define basic world / location data for each location (worldname,
			// x, y, z)
			int locationOneX = locationOne.getBlockX();
			int locationOneY = locationOne.getBlockY();
			int locationOneZ = locationOne.getBlockZ();
			String locationOneWorldName = locationOne.getWorld().getName();

			int locationTwoX = locationTwo.getBlockX();
			int locationTwoY = locationTwo.getBlockY();
			int locationTwoZ = locationTwo.getBlockZ();
			String locationTwoWorldName = locationTwo.getWorld().getName();

			// Set the locations to the configuration
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".1.x", locationOneX);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".1.y", locationOneY);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".1.z", locationOneZ);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".1.world", locationOneWorldName);

			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".2.x", locationTwoX);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".2.y", locationTwoY);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".2.z", locationTwoZ);
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".2.world", locationTwoWorldName);
			
			Envoy.getEnvoy().getEnvoyLocations().getConfig().set("locations." + args[1].toLowerCase() + ".max-chest", 25);
			
			// Save the configuration file.
			Envoy.getEnvoy().getEnvoyLocations().saveConfig();

			// Send a message saying that it was successfull
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.region-created").replace("{name}", args[1].toLowerCase())));

			
		}
	}
}
