package com.shadebyte.envoys.commands.subcmds;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class SummonEnvoyCMD extends SubCommand {

	public SummonEnvoyCMD() {
		super("start", "start", "Envoy.cmd.summonenvoy", "Summon Envoy", 1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (!(sender instanceof Player)) {
			return;
		}

		Player p = (Player) sender;

		for (String all : Envoy.getEnvoy().getConfig().getStringList("messages.envoy-by-player")) {
			Bukkit.broadcastMessage(ChatUtils.getChatUtils().color(all).replace("{player}", p.getName()));
		}
		EnvoyUtils.getUtils().spawnRandomizedEnvoy();
	}
}
