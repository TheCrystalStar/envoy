package com.shadebyte.envoys.commands.subcmds;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.Maths;

public class GiveCMD extends SubCommand {

	public GiveCMD() {
		super("give", "give", "Envoy.cmd.give", "give an item", 4);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (args.length == 1) {
			sender.sendMessage(ChatUtils.getChatUtils().color("&e/envoy give <player> flare <#>"));
		}

		if (args.length == 2) {

			Player target = Bukkit.getPlayerExact(args[1]);

			if (target != null) {
				sender.sendMessage(ChatUtils.getChatUtils().color("&e/envoy give " + args[1] + " flare &c<#>"));
			} else {
				sender.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.player-offline")));
			}
		}

		if (args.length == 3) {

			Player target = Bukkit.getPlayerExact(args[1]);

			if (target != null) {
				sender.sendMessage(ChatUtils.getChatUtils().color("&e/envoy give " + args[1] + " flare &c<#>"));
			} else {
				sender.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.player-offline")));
			}
		}

		if (args.length == 4) {

			Player target = Bukkit.getPlayerExact(args[1]);

			if (target != null) {

				if (Maths.getMaths().isInt(args[3])) {

					int amt = Integer.parseInt(args[3]);

					for (int i = 0; i < amt; i++) {
						target.getInventory().addItem(Envoy.getEnvoy().createItemStack("summonstick"));
					}
					
					sender.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.gavesummonstick").replace("{amt}", String.valueOf(amt)).replace("{player}", args[1])));
					target.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.summonstickadded").replace("{amt}", String.valueOf(amt))));

				} else {
					sender.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.notanumber")));
				}

			} else {
				sender.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.player-offline")));
			}
		}
	}
}
