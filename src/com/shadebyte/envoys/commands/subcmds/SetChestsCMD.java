package com.shadebyte.envoys.commands.subcmds;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class SetChestsCMD extends SubCommand {

	public SetChestsCMD() {
		super("setchests", "setchests", "Envoy.cmd.setchests", "Set the chest location.", 1);
	}

	private GameMode gm;

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (!(sender instanceof Player)) {
			return;
		}

		Player p = (Player) sender;

		if (Envoy.getEnvoy().settingChestLocations.contains(p)) {
			Envoy.getEnvoy().settingChestLocations.remove(p);
			p.getInventory().clear();
			p.getInventory().setContents(Envoy.getEnvoy().playerInventory.get(p.getUniqueId()));
			p.sendMessage(ChatUtils.getChatUtils().color("&e&l(!) &eYou are no longer setting the chest locations!"));
			Envoy.getEnvoy().playerInventory.remove(p.getUniqueId());
			p.setGameMode(gm);

			/*
			 * Looping through all the locations saved when setting the chest.
			 */
			for (Location loc : Envoy.getEnvoy().placedChest) {

				if (Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest.1") == null) {
					int x = loc.getBlockX();
					int y = loc.getBlockY();
					int z = loc.getBlockZ();
					String world = loc.getWorld().getName();
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest.1.x", x);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest.1.y", y);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest.1.z", z);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest.1.world", world);
				} else {
					ConfigurationSection sec = Envoy.getEnvoy().getChestConfig().getConfig().getConfigurationSection("chest");
					ArrayList<Integer> highs = new ArrayList<>();
					for (String keys : sec.getKeys(false)) {
						highs.add(Integer.parseInt(keys));
					}

					int max = Collections.max(highs);
					int x = loc.getBlockX();
					int y = loc.getBlockY();
					int z = loc.getBlockZ();
					String world = loc.getWorld().getName();
					max = max + 1;
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest." + max + ".x", x);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest." + max + ".y", y);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest." + max + ".z", z);
					Envoy.getEnvoy().getChestConfig().getConfig().set("chest." + max + ".world", world);
				}
			}
			Envoy.getEnvoy().getChestConfig().saveConfig();
			Envoy.getEnvoy().placedChest.clear();
			p.sendMessage(ChatUtils.getChatUtils().color("&e&l(!) &eAll chest locations saved to configuration!"));
			EnvoyUtils.getUtils().hideChestLocations();
			return;
		}

		Envoy.getEnvoy().settingChestLocations.add(p);
		gm = p.getGameMode();
		p.setGameMode(GameMode.CREATIVE);
		p.sendMessage(ChatUtils.getChatUtils().color("&e&l(!) &eYou are now setting the chest locations!"));
		Envoy.getEnvoy().playerInventory.put(p.getUniqueId(), p.getInventory().getContents());
		p.getInventory().clear();
		p.getInventory().setItem(4, Envoy.getEnvoy().createItemStack("settingchest.placeblock"));
		p.getInventory().setItem(0, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(1, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(2, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(3, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(5, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(6, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(7, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setItem(8, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
		p.getInventory().setHeldItemSlot(4);
		EnvoyUtils.getUtils().showChestLocations();

	}
}
