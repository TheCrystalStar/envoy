package com.shadebyte.envoys.commands.subcmds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;

public class AddItemCMD extends SubCommand {

	public AddItemCMD() {
		super("additem", "additem", "Envoy.cmd.additem", "Add an item to the envoy chance wheel", 1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (!(sender instanceof Player)) {
			return;
		}

		Player p = (Player) sender;
		
		//Check if their hand is empty
		if (p.getItemInHand().getType() == Material.AIR || p.getItemInHand() == null || p.getItemInHand().getType() == null) {
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.empty-hand")));
			return;
		}
		
		/**
		 * Add the actual item to the list.
		 */
		if (!Envoy.getEnvoy().getItemsConfig().getConfig().contains("items")) {
			ItemStack item = p.getItemInHand();
			ArrayList<ItemStack> items = new ArrayList<>();
			items.add(item);
			ItemStack[] fitems = items.toArray(new ItemStack[0]);
			Envoy.getEnvoy().getItemsConfig().getConfig().set("items", fitems);
			Envoy.getEnvoy().getItemsConfig().saveConfig();
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.added-item")));
			Envoy.getEnvoy().getItemsConfig().reloadConfig();

		} else {
			ItemStack item = p.getItemInHand();
			@SuppressWarnings("unchecked")
			List<ItemStack> items = (List<ItemStack>) Envoy.getEnvoy().getItemsConfig().getConfig().get("items");
			items.add(item);
			ItemStack[] fitems = items.toArray(new ItemStack[0]);
			Envoy.getEnvoy().getItemsConfig().getConfig().set("items", fitems);
			Envoy.getEnvoy().getItemsConfig().saveConfig();
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.added-item")));
			Envoy.getEnvoy().getItemsConfig().reloadConfig();

		}
	}
}
