package com.shadebyte.envoys.commands.subcmds;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;

public class SelectRegionCMD extends SubCommand {

	public SelectRegionCMD() {
		super("selectregion", "selectregion", "Envoy.cmd.selectregion", "create region", 1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (!(sender instanceof Player)) {
			return;
		}
		
		Player p = (Player) sender;

		if (!Envoy.getEnvoy().creatingRegion.contains(p)) {
			Envoy.getEnvoy().creatingRegion.add(p);
			p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.entered-region-creation")));
			return;
		}
		
		Envoy.getEnvoy().creatingRegion.remove(p);
		p.sendMessage(ChatUtils.getChatUtils().color(Envoy.getEnvoy().getConfig().getString("messages.exited-region-creation")));
	}
}
