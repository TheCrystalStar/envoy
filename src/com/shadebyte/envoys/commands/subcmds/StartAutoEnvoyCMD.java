package com.shadebyte.envoys.commands.subcmds;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.commands.SubCommand;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class StartAutoEnvoyCMD extends SubCommand {

	public StartAutoEnvoyCMD() {
		super("startauto", "startauto", "Envoy.cmd.startauto", "start auto Envoy", 1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (Envoy.getEnvoy().getConfig().getBoolean("chest.randomized-locations")) {
			for (String all : Envoy.getEnvoy().getConfig().getStringList("messages.envoy-start")) {
				Bukkit.broadcastMessage(ChatUtils.getChatUtils().color(all));
			}
			EnvoyUtils.getUtils().spawnRandomizedEnvoy();
		} else {
			for (String all : Envoy.getEnvoy().getConfig().getStringList("messages.envoy-start")) {
				Bukkit.broadcastMessage(ChatUtils.getChatUtils().color(all));
			}
			EnvoyUtils.getUtils().spawnControlledEnvoy();
		}
	}
}
