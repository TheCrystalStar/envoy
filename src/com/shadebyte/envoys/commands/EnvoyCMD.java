package com.shadebyte.envoys.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.envoys.commands.subcmds.AddItemCMD;
import com.shadebyte.envoys.commands.subcmds.CreateRegionCMD;
import com.shadebyte.envoys.commands.subcmds.GiveCMD;
import com.shadebyte.envoys.commands.subcmds.SelectRegionCMD;
import com.shadebyte.envoys.commands.subcmds.SetChestsCMD;
import com.shadebyte.envoys.commands.subcmds.StartAutoEnvoyCMD;
import com.shadebyte.envoys.commands.subcmds.SummonEnvoyCMD;
import com.shadebyte.envoys.utils.ChatUtils;

public class EnvoyCMD extends BaseCommand {

	private final Map<String, SubCommand> children = new HashMap<>();

	/**
	 * This is the main constructor that takes in the permission, command.
	 */
	public EnvoyCMD() {
		super("Envoy", "Envoy.cmd");
		children.put("setchests", new SetChestsCMD());
		children.put("startauto", new StartAutoEnvoyCMD());
		children.put("start", new SummonEnvoyCMD());
		children.put("create", new CreateRegionCMD());
		children.put("selectregion", new SelectRegionCMD());
		children.put("additem", new AddItemCMD());
		children.put("give", new GiveCMD());
	}

	/**
	 * This is called when the player/sender runs the command /envoy
	 */
	@Override
	public void execute(CommandSender sender, String[] args) {

		if (sender instanceof Player) {

			if(args.length == 0) {
				sender.sendMessage(ChatUtils.getChatUtils().color("&e============= &6Envoy &e============="));
				sender.sendMessage(ChatUtils.getChatUtils().color(""));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy additem &7-Adds item  in your hand to item list"));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy create <name> &7-Create a new envoy location"));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy selectregion &7-Select the corners to make an envoy region."));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy start &7-Start an evoy"));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy give <Player> flare <#> &7-Give a summon flare/stick to a player"));
				sender.sendMessage(ChatUtils.getChatUtils().color("&6/Envoy setchests &7-Set exact chest locations for non-randomized locations."));
				sender.sendMessage(ChatUtils.getChatUtils().color(""));
				return;
			}
			
		}
		
		try {
			SubCommand child = children.get(args[0].toLowerCase());

			if (child != null) {
				child.execute(sender, args);
				return;
			}
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public boolean registerChildren(SubCommand command) {

		if (children.get(command.getName()) != null) {
			return false;
		}

		children.put(command.getName(), command);
		return true;
	}
}
