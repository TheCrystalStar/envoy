package com.shadebyte.envoys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.sound.midi.Patch;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.shadebyte.envoy.runnables.ParticlesRunnable;
import com.shadebyte.envoys.commands.BaseCommand;
import com.shadebyte.envoys.commands.EnvoyCMD;
import com.shadebyte.envoys.events.ChestSetEvents;
import com.shadebyte.envoys.events.EnvoyLocationEvents;
import com.shadebyte.envoys.events.PunchChestEvents;
import com.shadebyte.envoys.utils.ChatUtils;
import com.shadebyte.envoys.utils.ConfigWrapper;
import com.shadebyte.envoys.utils.EnvoyUtils;
import com.shadebyte.envoys.utils.Maths;

public class Envoy extends JavaPlugin {

	/**
	 * Create a private instance for the class
	 */
	public static Envoy envoy;

	/**
	 * Create required configuration files.
	 */
	private ConfigWrapper chestLocations = new ConfigWrapper(this, "", "Chest.yml");
	private ConfigWrapper envoyLocations = new ConfigWrapper(this, "", "Locations.yml");
	private ConfigWrapper itemsConfig = new ConfigWrapper(this, "", "Items.yml");

	// These are used to store the players previous armor and inventory.
	public HashMap<UUID, ItemStack[]> playerInventory = new HashMap<>();

	public ArrayList<Player> settingChestLocations = new ArrayList<>();
	public ArrayList<Location> placedChest = new ArrayList<>();

	public ArrayList<Player> creatingRegion = new ArrayList<>();
	public HashMap<Player, Location> regionCornerOne = new HashMap<>();
	public HashMap<Player, Location> regionCornerTwo = new HashMap<>();
	public static HashMap<String, BukkitRunnable> cooldownTask = new HashMap<String, BukkitRunnable>();

	/**
	 * Called when the plugin is being enabled
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		
		// Set the instance = to this class.
		envoy = this;

		// Create the default configuration and save the defaults
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();

		if (!chestLocations.getConfig().contains("chest")) {
			chestLocations.getConfig().createSection("chest");
			chestLocations.saveConfig();
		}

		envoyLocations.reloadConfig();
		envoyLocations.saveConfig();

		itemsConfig.reloadConfig();
		itemsConfig.saveConfig();

		EnvoyUtils.getUtils().loadPlacedLocations();

		// Register the events.
		Bukkit.getPluginManager().registerEvents(new ChestSetEvents(), this);
		Bukkit.getPluginManager().registerEvents(new EnvoyLocationEvents(), this);
		Bukkit.getPluginManager().registerEvents(new PunchChestEvents(), this);

		/**
		 * Register all the BaseCommands as normal commands, sub command
		 * registration happens "automatically"
		 */
		List<BaseCommand> commands = Arrays.asList(new EnvoyCMD());
		for (BaseCommand command : commands) {
			getCommand(command.getName()).setExecutor(command);
		}

		 Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new
		 BukkitRunnable() {
		
		 @Override
		 public void run() {
		 Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "envoy startauto");
		 }
		
		 }, 20 * getConfig().getInt("spawn-times.delaytostart"),
		 Maths.getMaths().randInt(20 *
		 getConfig().getInt("spawn-times.minspawn"), 20 *
		 getConfig().getInt("spawn-times.maxspawn")));
		
		 new ParticlesRunnable().runTaskTimer(this, 0, 20);
	}

	/**
	 * Called when the plugin is being disbaled
	 */
	@Override
	public void onDisable() {
	}

	/**
	 * This getter method is used to get the instance of this main class "Envoy"
	 */
	public static Envoy getEnvoy() {
		return envoy;
	}

	/**
	 * Create the getter method for the configuration files.
	 */
	public ConfigWrapper getChestConfig() {
		return chestLocations;
	}

	public ConfigWrapper getEnvoyLocations() {
		return envoyLocations;
	}

	public ConfigWrapper getItemsConfig() {
		return itemsConfig;
	}

	/**
	 * This method is used to create an itemstack from a value in the default
	 * config.yml
	 */
	public ItemStack createItemStack(String item) {
		ItemStack is = new ItemStack(Material.APPLE, 1);
		is.setType(Material.getMaterial(getConfig().getString("items." + item + ".id")));
		is.setDurability((short) getConfig().getInt("items." + item + ".meta"));
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatUtils.getChatUtils().color(getConfig().getString("items." + item + ".name")));
		ArrayList<String> lore = new ArrayList<>();
		for (String all : getConfig().getStringList("items." + item + ".lore")) {
			lore.add(ChatUtils.getChatUtils().color(all));
		}
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
}
