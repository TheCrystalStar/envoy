package com.shadebyte.envoy.runnables;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

import com.shadebyte.envoys.Envoy;
import com.shadebyte.envoys.utils.EnvoyUtils;

public class ParticlesRunnable extends BukkitRunnable {

	@Override
	public void run() {
		for (Location location : Envoy.getEnvoy().placedChest) {
			Location loc = location.clone();
			if (location.getBlock().getType() == Material.CHEST) {
				location.getWorld().playEffect(loc.add(0, 1, 0), Effect.MOBSPAWNER_FLAMES, 1);
			}
		}
		for (Location loc : EnvoyUtils.getUtils().spawnedChest) {
			Material spawnedChest = loc.getBlock().getType();
			Location location = loc.clone();
			if (spawnedChest.equals(Material.CHEST)) {
				loc.getWorld().playEffect(location.add(0, 1, 0), Effect.MOBSPAWNER_FLAMES, 1);
			}
		}
	}
}
